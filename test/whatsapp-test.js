'use strict'

const test = require('ava')
const { sendMessage } = require('../src/utils/whatsapp')

test('Send whatsapp message', async t => {
  const result = await sendMessage('¿Qué hay?', '+593983683624')
  t.is(result, true)
})
