'use strict'

const { BadRequestException } = require('../exceptions/exceptions')
const config = require('../../config').config
const twilio = require('twilio')
const client = twilio(config.twilio.account_sid, config.twilio.auth_token)

const sendMessage = function (body, number) {
  return new Promise((resolve, reject) => {
    client.messages
      .create({
        body: body,
        from: 'whatsapp:+14155238886',
        to: `whatsapp:${number}`
      })
      .then(message => {
        console.log(message)
        resolve(true)
      })
      .catch(err => {
        console.log(err)
        reject(new BadRequestException('Mensaje no enviado.'))
      })
  })
}

module.exports = { sendMessage }
